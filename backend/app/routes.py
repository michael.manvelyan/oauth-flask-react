from flask import request, redirect

from app.helpers import *
from app.blueprints import api
from app.decorators import token_required


@api.route('/auth/google', methods=['POST'])
def auth_google():
    if request.method == 'POST':
        return auth_google_helper(request)


@api.route('/auth/facebook', methods=['POST'])
@token_required
def auth_facebook():
    if request.method == 'POST':
        return auth_facebook_helper(request)


@api.route('/auth/linkedin', methods=['POST'])
@token_required
def auth_linkedin():
    if request.method == 'POST':
        return auth_linkedin_helper(request)


@api.route('/auth/twitter')
def auth_twitter():
    return redirect(auth_twitter_url())


@api.route('/twitter/callback')
def twitter_callback():
    account = callback_twitter_helper(request)
    return jsonify(account)


@api.route('/users')
@token_required
def get_users():
    return get_users_helper(request)


@api.route('/users/me')
@token_required
def get_current_user():
    return get_current_user_helper(request)

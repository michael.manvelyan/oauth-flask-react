from app.extensions import admin, db
from flask_admin.contrib.sqla import ModelView

from app.models import User


class CustomView(ModelView):
    column_display_pk = True


def create_admin():
    admin.add_view(CustomView(User, db.session))

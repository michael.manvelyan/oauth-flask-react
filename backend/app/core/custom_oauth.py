from rauth import OAuth1Service, OAuth2Service
from flask import abort, current_app, url_for, request, redirect, session
import json
import random
import string


def jsondecoder(content):
    try:
        return json.loads(content.decode('utf-8'))
    except Exception as e:
        print(e)
        return json.loads(content)


class OAuthSignIn(object):
    providers = None

    def __init__(self, provider_name):
        self.provider_name = provider_name
        credentials = current_app.config['OAUTH_CREDENTIALS'][provider_name]
        self.consumer_id = credentials['id']
        self.consumer_secret = credentials['secret']
        self.state = ''.join(random.choice(string.ascii_uppercase) for i in range(10))

    def authorize(self):
        pass

    def get_callback_url(self):
        return url_for('api.oauth_callback', provider=self.provider_name, _external=True)

    def callback(self):
        pass

    def validate_oauth2callback(self):
        if 'code' not in request.args:  # dump request if problem
            abort(500, 'oauth2 callback: code not in request.args: \n' + str(request.__dict__))
        if request.args.get('state') != session.get('state'):
            print('state', request.args.get('state'))
            abort(500, 'oauth2 callback: state does not match: \n' + str(request.__dict__))

    @classmethod
    def get_provider(self, provider_name):
        if self.providers is None:
            self.providers = {}
            for provider_class in self.__subclasses__():
                provider = provider_class()
                self.providers[provider.provider_name] = provider
        return self.providers[provider_name]


class GoogleSignIn(OAuthSignIn):
    def __init__(self):
        super(GoogleSignIn, self).__init__('google')
        self.service = OAuth2Service(
            name='google',
            client_id=self.consumer_id,
            client_secret=self.consumer_secret,
            authorize_url='https://accounts.google.com/o/oauth2/v2/auth',
            access_token_url='https://www.googleapis.com/oauth2/v4/token',
            base_url='https://www.googleapis.com/oauth2/v3/userinfo'
        )

    def authorize(self):
        return redirect(self.service.get_authorize_url(
            scope='email',
            response_type='code',
            redirect_uri=self.get_callback_url(),
            state=self.state)
        )

    def callback(self):
        self.validate_oauth2callback()
        # get token
        oauth_session = self.service.get_auth_session(
            data={'code': request.args['code'],
                  'grant_type': 'authorization_code',
                  'redirect_uri': self.get_callback_url()
                  },
            decoder=jsondecoder
        )
        me = oauth_session.get('').json()
        social_id = 'google$' + me['sub']
        nickname = me.get('name', None) if me.get('name', None) else me['email'].split('@')[0]
        email = me['email'] if me['email_verified'] == True else None
        url = me.get('profile', None)
        return social_id, nickname, email


class TwitterSignIn(OAuthSignIn):
    def __init__(self):
        super(TwitterSignIn, self).__init__('twitter')
        self.service = OAuth1Service(
            name='twitter',
            consumer_key=self.consumer_id,
            consumer_secret=self.consumer_secret,
            request_token_url='https://api.twitter.com/oauth/request_token',
            authorize_url='https://api.twitter.com/oauth/authorize',
            access_token_url='https://api.twitter.com/oauth/access_token',
            base_url='https://api.twitter.com/1.1/'
        )

    def authorize(self):
        request_token = self.service.get_request_token(
            params={'oauth_callback': self.get_callback_url()}
        )
        session['request_token'] = request_token
        return redirect(self.service.get_authorize_url(request_token[0]))

    def callback(self):
        request_token = session.pop('request_token')
        if 'oauth_verifier' not in request.args:
            return None, None, None
        oauth_session = self.service.get_auth_session(
            request_token[0],
            request_token[1],
            data={'oauth_verifier': request.args['oauth_verifier']}
        )
        me = oauth_session.get('account/verify_credentials.json').json()
        social_id = 'twitter$' + str(me.get('id'))
        nickname = me.get('screen_name')
        email = None
        url = 'https://twitter.com/' + me.get('screen_name')
        return social_id, nickname, email

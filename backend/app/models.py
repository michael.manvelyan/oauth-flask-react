import jwt
import datetime

from app.extensions import db
from app import config


class Base(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    create_datetime = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def save(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def get_all(cls):
        return cls.query.all()


class User(Base):
    google_id = db.Column(db.String)
    google_first_name = db.Column(db.String)
    google_last_name = db.Column(db.String)
    google_email = db.Column(db.String)
    google_name = db.Column(db.String)
    google_avatar = db.Column(db.String)
    google_locale = db.Column(db.String)

    facebook_id = db.Column(db.String)
    facebook_name = db.Column(db.String)
    facebook_email = db.Column(db.String)
    facebook_avatar = db.Column(db.String)

    linkedin_id = db.Column(db.String)
    linkedin_name = db.Column(db.String)
    linkedin_email = db.Column(db.String)
    linkedin_avatar = db.Column(db.String)

    @classmethod
    def get_by_google_id(cls, google_id):
        return cls.query.filter_by(google_id=google_id).first()

    @staticmethod
    def encode_auth_token(user_id):
        """Generates the Auth Token"""
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=30, seconds=5),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(
                payload,
                config.secret_key,
                algorithm='HS256'
            ).decode('utf-8')
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """Decodes the auth token"""
        try:
            payload = jwt.decode(auth_token, config.secret_key)
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'

    def __repr__(self):
        return "<User '{}'>".format(self.google_name)

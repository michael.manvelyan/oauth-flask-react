import json
import requests
from flask import jsonify, session
from rauth import OAuth1Service

from app import config
from .models import User


twitterService = OAuth1Service(
    name='twitter',
    consumer_key=config.TWITTER_KEY,
    consumer_secret=config.TWITTER_SECRET,
    request_token_url='https://api.twitter.com/oauth/request_token',
    authorize_url='https://api.twitter.com/oauth/authorize',
    access_token_url='https://api.twitter.com/oauth/access_token',
    base_url='https://api.twitter.com/1.1/'
)


def get_logged_in_user(new_request):
    # get the auth token
    auth_token = new_request.headers.get('Authorization')
    if auth_token:
        resp = User.decode_auth_token(auth_token)
        if not isinstance(resp, str):
            user = User.query.filter_by(id=resp).first()

            response_object = {
                'status': 'success',
                'data': auth_token
            }
            return response_object, 200
        response_object = {
            'status': 'fail',
            'message': resp
        }
        return response_object, 401
    else:
        response_object = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return response_object, 401


def get_current_user(new_request):
    auth_token = new_request.headers.get('Authorization')
    resp = User.decode_auth_token(auth_token)
    return User.query.filter_by(id=resp).first()


def auth_google_helper(request):
    payload = request.json

    data = payload.get('profileObj')

    user = User.get_by_google_id(payload.get('googleId'))

    if not user:
        user = User(
            google_id=data.get('googleId'),
            google_email=data.get('email'),
            google_first_name=data.get('givenName'),
            google_last_name=data.get('familyName'),
            google_name=data.get('name'),
            google_avatar=data.get('imageUrl'),
            google_locale=data.get('locale')
        )
        user.save()

    response_object = {
        'token': User.encode_auth_token(user_id=user.id)
    }

    return response_object


def auth_facebook_helper(request):
    user = get_current_user(request)
    payload = request.json

    user.facebook_id = payload.get('id')
    user.facebook_name = payload.get('name')
    user.facebook_email = payload.get('email')
    user.facebook_avatar = payload.get('picture')['data']['url']
    user.save()

    response_object = {
        'status': 'success',
        'message': 'Facebook profile saved.'
    }

    return response_object


def auth_linkedin_helper(request):
    user = get_current_user(request)
    payload = request.json

    response = requests.post('https://www.linkedin.com/oauth/v2/accessToken', {
        'grant_type': 'authorization_code',
        'code': payload.get('code'),
        'redirect_uri': 'http://localhost:3000/linkedin',
        'client_id': config.LINKEDIN_CLIENT_ID,
        'client_secret': config.LINKEDIN_SECRET_KEY
    })

    content = json.loads(response.content)
    access_token = content.get('access_token')

    response = requests.get('https://api.linkedin.com/v2/me', headers={
        'x-li-format': 'json',
        'Authorization': 'Bearer ' + access_token
    })

    data = json.loads(response.content)

    user.linkedin_id = data.get('id')
    user.linkedin_name = data.get('localizedFirstName') + ' ' + data.get('localizedLastName')
    user.save()

    response_object = {
        'status': 'success',
        'message': 'Linkedin profile saved.'
    }

    return response_object


def auth_twitter_url():
    request_token = twitterService.get_request_token(
        params={'oauth_callback': 'http://localhost:5000/api/twitter/callback'}
    )

    session['request_token'] = request_token

    return twitterService.get_authorize_url(request_token[0])


def callback_twitter_helper(request):
    request_token = session.pop('request_token')

    oauth_session = twitterService.get_auth_session(
        request_token[0],
        request_token[1],
        data={'oauth_verifier': request.args['oauth_verifier']}
    )
    me = oauth_session.get('account/verify_credentials.json').json()

    return me


def get_users_helper(request):
    users = []
    for user in User.get_all():
        user_object = {
            'google_id': user.google_id,
            'google_first_name': user.google_first_name,
            'google_last_name': user.google_last_name,
            'google_email': user.google_email,
            'google_name': user.google_name,
            'google_avatar': user.google_avatar,
            'google_locale': user.google_locale,
            'facebook_id': user.facebook_id,
            'facebook_name': user.facebook_name,
            'facebook_email': user.facebook_email,
            'facebook_avatar': user.facebook_avatar,
            'linkedin_id': user.linkedin_id,
            'linkedin_name': user.linkedin_name,
            'linkedin_email': user.linkedin_email,
            'linkedin_avatar': user.linkedin_avatar
        }
        users.append(user_object)

    return jsonify(users)


def get_current_user_helper(request):
    user = get_current_user(request)

    response_object = {
        'google_id': user.google_id,
        'google_first_name': user.google_first_name,
        'google_last_name': user.google_last_name,
        'google_email': user.google_email,
        'google_name': user.google_name,
        'google_avatar': user.google_avatar,
        'google_locale': user.google_locale,
        'facebook_id': user.facebook_id,
        'facebook_name': user.facebook_name,
        'facebook_email': user.facebook_email,
        'facebook_avatar': user.facebook_avatar,
        'linkedin_id': user.linkedin_id,
        'linkedin_name': user.linkedin_name,
        'linkedin_email': user.linkedin_email,
        'linkedin_avatar': user.linkedin_avatar
    }

    return response_object

import os
from flask import Flask

from app.extensions import db, admin, cors
from app.admin import create_admin


def create_app():
    app = Flask(__name__)

    basedir = os.path.abspath(os.path.dirname(__file__))

    app.debug = True

    app.secret_key = 'secret-key'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+os.path.join(basedir, 'data.db')
    app.config['FLASK_ADMIN_SWATCH'] = 'flatly'

    db.init_app(app)
    admin.init_app(app)
    cors.init_app(app)

    create_admin()

    from app.routes import api
    app.register_blueprint(api)

    return app

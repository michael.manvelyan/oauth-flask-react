from .base import Base, db


class SocialUser(Base):
    social = db.Column(db.String(16))
    social_id = db.Column(db.String)
    nickname = db.Column(db.String)
    first_name = db.Column(db.String)
    middle_name = db.Column(db.String)
    last_name = db.Column(db.String)
    email = db.Column(db.String)
    dob = db.Column(db.DateTime)

    primary = db.Column(db.Boolean, default=False)

    # One to Many
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

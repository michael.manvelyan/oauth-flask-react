from functools import wraps
from flask import request
from app.helpers import get_logged_in_user


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        data, status = get_logged_in_user(request)
        token = data.get('data')
        if not token:
            return data, status

        return f(*args, **kwargs)

    return decorated

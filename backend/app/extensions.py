from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_cors import CORS


db = SQLAlchemy()
cors = CORS(resources={r"/api/*": {"origins": "*"}})
admin = Admin(
    name='Flask-Admin',
    template_mode='bootstrap3'
)


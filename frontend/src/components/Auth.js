import React from 'react'
import {GoogleLogin} from 'react-google-login';
import config from '../config.json';
import {register} from "../utils/api";


class Auth extends React.Component {
    onFailure = (error) => {
        alert(error);
    };

    googleResponse = (response) => {
        register('google', response, true)
            .then(response => {
                this.props.login(response.token);
            });
    };

    render() {
        return <div>
            <h1>Auth Page</h1>

            <GoogleLogin
                clientId={config.GOOGLE_CLIENT_ID}
                buttonText="Login"
                onSuccess={this.googleResponse}
                onFailure={this.onFailure}
            />
        </div>
    }
}

export default Auth

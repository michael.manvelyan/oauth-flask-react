import React from 'react'
import {getUsers} from "../utils/api";


class Members extends React.Component {
    state = {
        users: [],
        jwtToken: localStorage.getItem('jwtToken')
    };

    componentDidMount() {
        getUsers(this.state.jwtToken)
            .then(data => {
                this.setState({users: data});
            });
    };

    render() {
        let users = this.state.users,
            usersArr = [];

        const preStyle = {border: '1px solid pink'};

        for (const [index, user] of users.entries()) {
            usersArr.push(
                <pre style={preStyle} key={index}>
                    <b>Google Name</b>: {user.google_name} <br/>
                    <b>Google Email</b>: {user.google_email} <br/>
                    <b>Google Avatar</b>: <a href={user.google_avatar}>Link</a><br/>

                    <b>Facebook Name</b>: {user.facebook_name} <br/>
                    <b>Facebook Email</b>: {user.facebook_email} <br/>
                    <b>Facebook Avatar</b>: <a href={user.facebook_avatar}>Link</a><br/>

                    <b>LinkedIn Name</b>: {user.linkedin_name} <br/>
                    <b>LinkedIn Email</b>: {user.linkedin_email} <br/>
                    <b>LinkedIn Avatar</b>: {user.linkedin_avatar} <br/>
                </pre>
            )
        }

        return <div>
            <h1>Members Page</h1>
            {usersArr}
        </div>
    }
}

export default Members

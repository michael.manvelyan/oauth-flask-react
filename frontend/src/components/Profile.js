import React from 'react'
import FacebookLogin from 'react-facebook-login';
import {LinkedIn} from 'react-linkedin-login-oauth2';

import config from '../config.json';
import {register, getUser} from '../utils/api'


class Profile extends React.Component {
    state = {
        currentUser: null,
        jwtToken: localStorage.getItem('jwtToken')
    };

    successFacebook = (response) => {
        register('facebook', response, this.state.jwtToken);
        getUser(this.state.jwtToken)
            .then(data => {
                this.setState({currentUser: data});
            });
    };

    failFacebook = (error) => {
        alert(error)
    };

    successLinkedin = (response) => {
        register('linkedin', response, this.state.jwtToken);
        getUser(this.state.jwtToken)
            .then(data => {
                this.setState({currentUser: data});
            });
    };

    failLinkedin = (error) => {
        alert(error)
    };

    componentDidMount() {
        getUser(this.state.jwtToken)
            .then(data => {
                this.setState({currentUser: data});
            });
    };

    render() {
        let user = this.state.currentUser,
            userComponent = '';

        if (user) {
            const preStyle = {border: '1px solid black'};

            userComponent = <pre style={preStyle}>
                <b>Google Name</b>: {user.google_name} <br/>
                <b>Google Email</b>: {user.google_email} <br/>
                <b>Google Avatar</b>: <a href={user.google_avatar}>Link</a><br/>

                <b>Facebook Name</b>: {user.facebook_name} <br/>
                <b>Facebook Email</b>: {user.facebook_email} <br/>
                <b>Facebook Avatar</b>: <a href={user.facebook_avatar}>Link</a><br/>

                <b>LinkedIn Name</b>: {user.linkedin_name} <br/>
                <b>LinkedIn Email</b>: {user.linkedin_email} <br/>
                <b>LinkedIn Avatar</b>: {user.linkedin_avatar} <br/>
            </pre>
        }
        return <div>
            <h1>Profile Page</h1>
            <FacebookLogin
                appId={config.FACEBOOK_APP_ID}
                fields="name,email,picture"
                callback={this.successFacebook}/>

            <LinkedIn
                clientId={config.LINKEDIN_CLIENT_ID}
                onFailure={this.failLinkedin}
                onSuccess={this.successLinkedin}
                scope="r_liteprofile"
                redirectUri={config.APP_URL + '/linkedin'}/>

            <a href="http://localhost:5000/api/auth/twitter">Twitter</a>
            <br/>
            {userComponent}
        </div>
    }
}

export default Profile

import React, {Component} from 'react';
import jwtDecode from 'jwt-decode';
import Public from "./components/Public";
import Auth from "./components/Auth";
import Profile from "./components/Profile";
import Members from "./components/Members";
import NotFound from "./components/NotFound";
import requireAuth from "./utils/requireAuth";
import {LinkedInPopUp} from 'react-linkedin-login-oauth2';


import {
    Route,
    Link,
    Switch,
    BrowserRouter as Router
} from 'react-router-dom'


class App extends Component {
    state = {
        isAuthenticated: false
    };

    logout = () => {
        this.setState({isAuthenticated: false});
        localStorage.removeItem('jwtToken')
    };

    login = (token) => {
        this.setState({isAuthenticated: true});
        localStorage.setItem('jwtToken', token)
    };

    componentDidMount() {
        if (localStorage.jwtToken) {
            const {exp} = jwtDecode(localStorage.jwtToken);

            if (exp > Date.now() / 1000)
                this.setState({
                    isAuthenticated: true
                });
            else
                console.log("Authorization token expired");
        }
    };

    render() {
        return (
            <Router>
                <div>
                    <ul>
                        <li>
                            <Link to="/">Public</Link>
                        </li>
                        <li>
                            <Link to="/auth">Auth</Link>
                        </li>
                        <li>
                            <Link to="/profile">Profile</Link>
                        </li>
                        <li>
                            <Link to="/members">Members</Link>
                        </li>
                        <li>
                            <button onClick={this.logout}>Logout</button>
                        </li>
                    </ul>

                    <Switch>
                        <Route path="/" component={Public} exact/>
                        <Route path="/auth" render={(props) => <Auth {...props}
                                                                     isAuth={this.state.isAuthenticated}
                                                                     logout={this.logout}
                                                                     login={this.login}/>} exact/>
                        <Route path="/profile" component={requireAuth(Profile, this.state.isAuthenticated)}/>
                        <Route path="/members" component={requireAuth(Members, this.state.isAuthenticated)}/>
                        <Route path="/linkedin" component={LinkedInPopUp}/>
                        <Route component={NotFound}/>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;

import React, {Component} from 'react';


export default function (ComposedComponent, isAuth) {
    class Authenticate extends Component {

        componentDidMount() {
            if (!isAuth) {
                this.props.history.push('/auth');
            }
        }

        componentDidUpdate() {
            if (!isAuth) {
                this.props.history.push('/auth');
            }
        }

        render() {
            if (isAuth) {
                console.log('Auth: true');
                return (
                    <ComposedComponent/>
                )
            } else {
                console.log('Auth: false')
                return ''
            }
        }
    }

    return (Authenticate);
}

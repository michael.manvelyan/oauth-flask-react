import config from "../config";


export function register(social, response, token = '') {
    let options = {
        method: 'POST',
        body: JSON.stringify(response),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    };

    if (token.length) {
        options.headers.Authorization = token
    }

    return fetch(config.API_URL + '/api/auth/' + social, options)
        .then(r => {
            return r.json()
        })
}


export function getUser(token) {
    return fetch(config.API_URL + '/api/users/me', {
        method: 'GET',
        headers: {
            Authorization: token
        }
    }).then(r => {
        return r.json()
    })
}


export function getUsers(token) {
    return fetch(config.API_URL + '/api/users', {
        method: 'GET',
        headers: {
            Authorization: token
        }
    }).then(r => {
        return r.json()
    })
}

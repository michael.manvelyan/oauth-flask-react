```
$ git clone https://gitlab.com/michael.manvelyan/oauth-flask-react.git
$ cd oauth-flask-react/backend
```

## Backend
```
$ virtualenv venv -p python3

MacOS and Linux
$ source venv/bin/activate

Windows
$ venv\Script\activate

$ pip3 install -r requirements.txt
$ python3 manage.py runserver

```

## Frontend
```
$ cd oauth-flask-react/frontend
$ npm i
$ npm start
```
